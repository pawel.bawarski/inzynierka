package settings;

import alerts.AlertMaker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {


    @FXML
    private JFXPasswordField password2;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initDefaultValues();
    }

    @FXML
    private void handlerSaveButtonAction(ActionEvent event) {
        String uname = username.getText();
        String pass = password.getText();
        String pass2 = password2.getText();

        if(pass.equals(pass2)) {
            Preferances preferances = Preferances.getPreferences();
            preferances.setUsername(uname);
            preferances.setPassword(pass);

            Preferances.writePreferenceToFile(preferances);
            ((Stage)username.getScene().getWindow()).close();
        }
        else {
            AlertMaker.showErrorMessage("Błędne hasła","Niepoprawne hasło w powtórzeniu");
        }
    }

    @FXML
    private void handlerCancelButtonAction(ActionEvent event) {
        ((Stage)username.getScene().getWindow()).close();
    }

    private void initDefaultValues(){
        Preferances preferances = Preferances.getPreferences();
        username.setText(String.valueOf(preferances.getUsername()));
        password.setText(String.valueOf(preferances.getPassword()));
        password2.setText(String.valueOf(preferances.getPassword()));
    }
}
