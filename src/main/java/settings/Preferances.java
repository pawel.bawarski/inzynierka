package settings;

import alerts.AlertMaker;
import com.google.gson.Gson;
import org.apache.commons.codec.digest.DigestUtils;
import sun.security.provider.ConfigFile;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Preferances {
    public static final String CONFIG_FILE = "config.txt";
    String username;
    String password;

    public Preferances(){
        username = "admin";
        setPassword("admin");
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(password.length()<16){
            this.password = DigestUtils.sha1Hex(password);
        }else {
            this.password = password;
        }
    }

    public static void initConfig(){
        Writer writer = null;
        try {
        Preferances preferances = new Preferances();
        Gson gson = new Gson();
        writer = new FileWriter(CONFIG_FILE);
            gson.toJson(preferances, writer);
        } catch (IOException e) {
            Logger.getLogger(Preferances.class.getName()).log(Level.SEVERE,null, e);
        }finally {
            try {
                writer.close();
            }
            catch (IOException e){
                Logger.getLogger(Preferances.class.getName()).log(Level.SEVERE,null, e);
            }
        }
    }


    public static Preferances getPreferences(){
        Gson gson = new Gson();
        Preferances preferances = new Preferances();
        try {
            preferances = gson.fromJson(new FileReader(CONFIG_FILE), Preferances.class);
        }
        catch (FileNotFoundException e){
            initConfig();
            Logger.getLogger(Preferances.class.getName()).log(Level.SEVERE,null, e);
        }
        return preferances;
    }


    public static void writePreferenceToFile(Preferances preferances){
        Writer writer = null;
        try {
            Gson gson = new Gson();
            writer = new FileWriter(CONFIG_FILE);
            gson.toJson(preferances, writer);

            AlertMaker.showSimpleAlert("Succes","Settings updated");
        } catch (IOException e) {
            Logger.getLogger(Preferances.class.getName()).log(Level.SEVERE,null, e);
            AlertMaker.showErrorMessage(e,"Failed","Can't save configuration file");
        }finally {
            try {
                writer.close();
            }
            catch (IOException e){
                Logger.getLogger(Preferances.class.getName()).log(Level.SEVERE,null, e);
            }
        }
    }
}


