package membersAndProducts;

import javafx.beans.property.SimpleStringProperty;

public class Members {
    private  SimpleStringProperty name;
    private  SimpleStringProperty surname;
    private  SimpleStringProperty id;
    private  SimpleStringProperty function;
    private  SimpleStringProperty password;

    public Members(String name, String surname, String function, String id, String password) {
        this.name = new SimpleStringProperty(name);
        this.surname = new SimpleStringProperty(surname);
        this.id = new SimpleStringProperty(id);
        this.function = new SimpleStringProperty(function);
        this.password = new SimpleStringProperty(password);

    }
    public String getSurname() {
        return surname.get();
    }

    public String getName() {
        return name.get();
    }

    public String getId() {
        return id.get();
    }

    public String getFunction() {
        return function.get();
    }

    public String getPassword() {
        return password.get();
    }
}