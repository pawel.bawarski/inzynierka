package datebase;

import ui.listbook.ProductsListController;
import ui.listmember.MemberListController;

import javax.swing.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public final class DatabaseHandler {


    private static DatabaseHandler handler = null;

    private static final String DB_URL = "jdbc:derby:database;create=true";
    private static Connection conn = null;
    private static Statement stmt = null;

    private DatabaseHandler() {
        createConnection();
        setupBookTable();
        setupMemberTable();
        setupIssueTable();
    }

    public static DatabaseHandler getInstance(){
        if (handler == null){
            handler = new DatabaseHandler();
        }
        return handler;
    }

    private static void createConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            conn = DriverManager.getConnection(DB_URL);
        }
        catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Can't load database","Datebase Error" , JOptionPane.ERROR_MESSAGE);
           System.exit(0);
        }
    }

    private void setupBookTable(){
        String TABLE_NAME ="BOOK";
        try{
            stmt = conn.createStatement();
            DatabaseMetaData dbn = conn.getMetaData();
            ResultSet tables = dbn.getTables(null,null, TABLE_NAME.toUpperCase(),null);
            if(tables.next()){
                System.out.println("Table "+TABLE_NAME+" alreadey exists. Ready for go!");
            }
            else {
                stmt.execute("CREATE TABLE "+TABLE_NAME+"("
                +"    id varchar(200) primary key, \n"
                +"    title varchar(200),\n"
                +"    author varchar(200),\n"
                +"    publisher varchar(100), \n"
                +"    isAvail boolean default true)");
            }
        }
        catch (SQLException e){
            System.err.println(e.getMessage()+"... setup Database");
        }
        finally {
        }
    }

    private void setupMemberTable() {
        String TABLE_NAME ="MEMBER";
        try{
            stmt = conn.createStatement();
            DatabaseMetaData dbn = conn.getMetaData();
            ResultSet tables = dbn.getTables(null,null, TABLE_NAME.toUpperCase(),null);
            if(tables.next()){
                System.out.println("Table "+TABLE_NAME+" alreadey exists. Ready for go!");
            }
            else {
                stmt.execute("CREATE TABLE "+TABLE_NAME+"("
                        +"    id varchar(200) primary key, \n"
                        +"    name varchar(200),\n"
                        +"    surname varchar(200),\n"
                        +"    mobile varchar(20),\n"
                        +"    password varchar(50) )");
            }
        }
        catch (SQLException e){
            System.err.println(e.getMessage()+"... setup Database");
        }
        finally {
        }
    }

    public ResultSet execQuery(String query) {
        ResultSet resultSet;
        try{
            stmt = conn.createStatement();
            resultSet = stmt.executeQuery(query);
        }
        catch (SQLException e){
            System.out.println("Exeption at execQuery: dataHandler "+e.getLocalizedMessage());
            return null;
        }
        finally {
        }
        return resultSet;
    }

    void setupIssueTable(){
        String TABLE_NAME = "ISSUE";
        try{
            stmt = conn.createStatement();
            DatabaseMetaData databaseMetaData = conn.getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null,null,TABLE_NAME.toUpperCase(), null);
            if(resultSet.next()){
                System.out.println("Table "+TABLE_NAME + " already exists. Ready for go!");
            }else {
                stmt.execute("CREATE TABLE "+ TABLE_NAME +"(" +
                        "bookID varchar(200) primary key,\n" +
                        "memberID varchar(200),\n" +
                        "issueTime timestamp default CURRENT_TIMESTAMP,\n" +
                        "renew_count integer default 0,\n" +
                        "FOREIGN KEY (bookID) REFERENCES BOOK(id),\n" +
                        "FOREIGN KEY (memberID) REFERENCES MEMBER(id)" +
                        ")");
            }
        }
        catch (SQLException e){
            System.err.println(e.getMessage()+" ... setupDatebase");
        }finally {

        }
    }

    public boolean execAction(String query) {
        try {
            stmt = conn.createStatement();
            stmt.execute(query);
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Error Occured", JOptionPane.ERROR_MESSAGE);
            System.out.println("Exeption at execQuery: dataHandler " + e.getLocalizedMessage());
            return false;
        } finally {
        }
    }

    public boolean deleteBook(ProductsListController.Book book){
        try{
            String deleteStatment ="DELETE FROM BOOK WHERE ID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(deleteStatment);
            preparedStatement.setString(1,book.getId());
            int res = preparedStatement.executeUpdate();
            if(res ==1){
                return true;
            }
        }
        catch (SQLException e){
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);
        }
        return false;
    }

    public boolean isBookAlreadyIssued(ProductsListController.Book book){
        try{
            String checkstmt ="DELETE COUNT(*) FROM ISSUE WHERE bookID = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(checkstmt);
            preparedStatement.setString(1,book.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int count = resultSet.getInt(1);
                return (count>0);
            }
            int res = preparedStatement.executeUpdate();
            if(res ==1){
                return true;
            }
        }
        catch (SQLException e){
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);
        }
        return false;
    }

    public boolean updateBook(ProductsListController.Book book){
        try {
            String update = "UPDATE BOOK SET TITLE=?, AUTHOR=?, PUBLISHER=? WHERE ID=?";
            PreparedStatement preparedStatement = conn.prepareStatement(update);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getPlublisher());
            preparedStatement.setString(4, book.getId());
            int res = preparedStatement.executeUpdate();
            return (res>0);
        }
        catch (SQLException e){
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);

        }
        return false;
    }

    public boolean isMemberHasAnyBooks(MemberListController.Member member) {
        try {
            String checkstmt = "SELECT COUNT(*) FROM ISSUE WHERE memberID=?";
            PreparedStatement stmt = conn.prepareStatement(checkstmt);
            stmt.setString(1, member.getId());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                System.out.println(count);
                return (count > 0);
            }
        }
        catch (SQLException e) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);
        }
        return false;
    }

    public boolean deleteMember(MemberListController.Member member) {
        try {
            String deleteStatement = "DELETE FROM MEMBER WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(deleteStatement);
            stmt.setString(1, member.getId());
            int res = stmt.executeUpdate();
            if (res == 1) {
                return true;
            }
        }
        catch (SQLException e) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);
        }
        return false;
    }

    public boolean updateMember(MemberListController.Member member){
        try {
            String update = "UPDATE MEMBER SET NAME=?, SURNAME=?,  MOBILE=?, PASSWORD=? WHERE ID=?";
            PreparedStatement preparedStatement = conn.prepareStatement(update);
            preparedStatement.setString(1, member.getName());
            preparedStatement.setString(2, member.getSurname());
            preparedStatement.setString(3, member.getMobile());
            preparedStatement.setString(4, member.getPassword());
            preparedStatement.setString(5, member.getId());
            int res = preparedStatement.executeUpdate();
            return (res>0);
        }
        catch (SQLException e){
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null ,e);

        }
        return false;
    }
}