package ui.listmember;

import alerts.AlertMaker;
import datebase.DatabaseHandler;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import membersAndProducts.Members;
import ui.addbook.ProductsAddController;
import ui.addmember.MemberAddController;
import util.LibraryAssistantUtil;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MemberListController implements Initializable {

    ObservableList<Members> list = FXCollections.observableArrayList();
    @FXML
    private TableView<Members> tableView;
    @FXML
    private TableColumn<Members, String> nameCol;
    @FXML
    private TableColumn<Members, String> idCol;
    @FXML
    private TableColumn<Members, String> functionCol;
    @FXML
    private TableColumn<Members, String> surnameCol;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initCol();
        loadData();
    }

    private void initCol() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        surnameCol.setCellValueFactory(new PropertyValueFactory<>("surname"));
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        functionCol.setCellValueFactory(new PropertyValueFactory<>("function"));
    }

    private void loadData() {

        list.clear();
        DatabaseHandler handler = DatabaseHandler.getInstance();
        String qu = "SELECT * FROM MEMBER";
        ResultSet rs = handler.execQuery(qu);
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String id = rs.getString("id");
                String function = rs.getString("function");
                String password = rs.getString("password");

                list.add(new Members(name, surname, id, function, password));

            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsAddController.class.getName()).log(Level.SEVERE, null, ex);
        }

        tableView.setItems(list);
    }

    public void handlerRefresh(ActionEvent event) {
        loadData();
    }

    public void handlerMemberEdit(ActionEvent event) {
        Members selectedForEdition = tableView.getSelectionModel().getSelectedItem();

        if(selectedForEdition == null){
            AlertMaker.showErrorMessage("No book selected", "Please select a member for edit");
            return;
        }

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/addmember/member_add.fxml"));
            Parent parent = loader.load();
            MemberAddController controller = (MemberAddController) loader.getController();
            controller.inflateUI(selectedForEdition);
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle("Edit Member");
            stage.setScene(new Scene(parent));
            stage.show();
            LibraryAssistantUtil.setStageIcon(stage);
            stage.setOnCloseRequest((e)->{
                handlerRefresh(new ActionEvent());
            });
        }
        catch (IOException e){
            Logger.getLogger(ProductsAddController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void handlerMemberDelete(ActionEvent event) {
        MemberListController.Member selectedForDeletion = tableView.getSelectionModel().getSelectedItem();
        if (selectedForDeletion == null) {
            AlertMaker.showErrorMessage("No member selected", "Please select a member for deletion.");
            return;
        }
        if (DatabaseHandler.getInstance().isMemberHasAnyBooks(selectedForDeletion)) {
            AlertMaker.showErrorMessage("Cant be deleted", "This member has some books.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deleting book");
        alert.setContentText("Are you sure want to delete " + selectedForDeletion.getName() + " ?");
        Optional<ButtonType> answer = alert.showAndWait();
        if (answer.get() == ButtonType.OK) {
            Boolean result = DatabaseHandler.getInstance().deleteMember(selectedForDeletion);
            if (result) {
                AlertMaker.showSimpleAlert("Book deleted", selectedForDeletion.getName() + " was deleted successfully.");
                list.remove(selectedForDeletion);
            } else {
                AlertMaker.showSimpleAlert("Failed", selectedForDeletion.getName() + " could not be deleted");
            }
        } else {
            AlertMaker.showSimpleAlert("Deletion cancelled", "Deletion process cancelled");
        }
    }


}
