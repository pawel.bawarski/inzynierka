package ui.addmember;

import alerts.AlertMaker;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import datebase.DatabaseHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ui.listmember.MemberListController;

import java.net.URL;
import java.util.ResourceBundle;

public class MemberAddController implements Initializable {


    DatabaseHandler databaseHandler;

    @FXML
    private AnchorPane rootPane;
    @FXML
    private JFXTextField name;
    @FXML
    private JFXTextField surname;
    @FXML
    private JFXTextField nrWorker;
    @FXML
    private JFXTextField password;
    @FXML
    private JFXTextField function;

    @FXML
    private JFXButton saveButton;
    @FXML
    private JFXButton cancelButton;

    private Boolean isEditMode = false;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    databaseHandler = DatabaseHandler.getInstance();
    }


    @FXML
    private void addMember(ActionEvent actionEvent) {
        String mName = name.getText();
        String mSurname = surname.getText();
        String mID = nrWorker.getText();
        String mfunction = function.getText();
        String mPassword = password.getText();


        if ( mName.isEmpty() ||mSurname .isEmpty()|| mID.isEmpty() ||  mPassword .isEmpty()) {
            AlertMaker.showErrorMessage("Can't", "Please Enter in all fields");
            return;
        }

        if(isEditMode){
            handleUpdateMember();
            return;
        }

        String st = "INSERT INTO MEMBER(id, name, surname, mobile, password) VALUES ("+
                "'"+mID+"',"+
                "'"+mName+"',"+
                "'"+mSurname+"',"+
                "'"+mfunction+"',"+
                "'"+mPassword+"')";
        System.out.println(st);
        if(databaseHandler.execAction(st)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Saved");
            alert.showAndWait();
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Error Occured");
            alert.showAndWait();
        }
    }

    @FXML
    private void cancel(ActionEvent actionEvent) {
        Stage stage = (Stage) rootPane.getScene().getWindow();
        stage.close();
    }

    public void inflateUI(MemberListController.Members member){
        name.setText(member.getName());
        surname.setText(member.getSurname());
        nrWorker.setText(member.getId());
        nrWorker.setEditable(false);
        mobile.setText(member.getMobile());
        password.setText(member.getPassword());

        isEditMode = true;
    }

    private void handleUpdateMember(){
        MemberListController.Member member = new MemberListController.Member(name.getText(),surname.getText(),nrWorker.getText(),mobile.getText(),password.getText());
        if(DatabaseHandler.getInstance().updateMember(member)){
            AlertMaker.showSimpleAlert("Success","Member Updated");
        }
        else {
            AlertMaker.showSimpleAlert("Failed", "Can't update member");
        }
    }
}
