package ui.addbook;

import alerts.AlertMaker;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import datebase.DatabaseHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ui.listbook.BookListController;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookAddController implements Initializable {

    @FXML
    private JFXTextField title;
    @FXML
    private JFXTextField id;
    @FXML
    private JFXTextField author;
    @FXML
    private JFXTextField publisher;
    @FXML
    private JFXButton saveButton;
    @FXML
    private JFXButton cancelButton;
    @FXML
    private AnchorPane rootPane;

    private DatabaseHandler databaseHandler;
    private Boolean isEditMode = false;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        databaseHandler = DatabaseHandler.getInstance();
        checkdata();
    }

    @FXML
    public void addBook(ActionEvent actionEvent) {
        String bookID = id.getText();
        String bookAuthor= author.getText();
        String bookTitle = title.getText();
        String bookPublisher = publisher.getText();
        if(bookID.isEmpty()||bookAuthor.isEmpty()||bookTitle.isEmpty()||bookPublisher.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Please Enter in all fields");
            alert.showAndWait();
            return;
        }

        if(isEditMode){
            handleEditOperation();
            return;
        }
        String query ="INSERT INTO BOOK VALUES (" +
                "'"+bookID+"',"+
                "'"+bookTitle+"',"+
                "'"+bookAuthor+"',"+
                "'"+bookPublisher+"'," +
                ""+true+")";
        if(databaseHandler.execAction(query)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Success");
            alert.showAndWait();
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Failed");
            alert.showAndWait();
        }
    }


    @FXML
    public void cancel(ActionEvent actionEvent) {
        Stage stage = (Stage) rootPane.getScene().getWindow();
        stage.close();
    }

    private void checkdata() {
        String query ="SELECT title FROM BOOK";
        ResultSet resultSet = databaseHandler.execQuery(query);
        try{
            while (resultSet.next()){
                String titlex = resultSet.getString("title");
            }
        }
        catch (SQLException e){
            Logger.getLogger(BookAddController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void inflateUI(BookListController.Book book){
        title.setText(book.getTitle());
        id.setText(book.getId());
        author.setText(book.getAuthor());
        publisher.setText(book.getPlublisher());
        id.setEditable(false);
        isEditMode = true;
    }

    private void handleEditOperation() {
//        BookListController.Book book = new BookListController.Book(title.getText(),id.getText(),author.getText(),publisher.getText(),true);
//        if(databaseHandler.updateBook(book)){
//            AlertMaker.showSimpleAlert("Success","Book Updated");
//        }
//        else {
//            AlertMaker.showSimpleAlert("Failed", "Can't update book");
//        }
    }
}
