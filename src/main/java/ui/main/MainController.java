package ui.main;

import com.jfoenix.controls.JFXTextField;
import com.jfoenix.effects.JFXDepthManager;
import datebase.DatabaseHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import util.LibraryAssistantUtil;


import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private ListView<String> issueDataList;
    @FXML
    private JFXTextField bookID;
    @FXML
    private  TextField memberIDInput;
    @FXML
    private Text memberName;
    @FXML
    private Text memberMobile;
    @FXML
    private TextField bookIDInput;
    @FXML
    private Text bookName;
    @FXML
    private Text bookAuthor;
    @FXML
    private Text bookStatus;
    @FXML
    private HBox bookInfo;
    @FXML
    private HBox memberInfo;

    private Boolean isReadyForSubmission = false;

    DatabaseHandler databaseHandler;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        JFXDepthManager.setDepth(bookInfo,1);
        JFXDepthManager.setDepth(memberInfo,1);

        databaseHandler = DatabaseHandler.getInstance();
        initDrawer();
    }

    @FXML
    public void loadAddMember(ActionEvent actionEvent) {
        loadWindow("/fxml/addmember/member_add.fxml","Dodaj nowego użytkownika");
    }

    @FXML
    public void loadMemberTable(ActionEvent actionEvent) {
        loadWindow("/fxml/listmember/member_list.fxml","Lista użytkowników");
    }

    @FXML
    public void loadBookTable(ActionEvent actionEvent) {
        loadWindow("/fxml/listbook/products_list.fxml","Book List");
    }

    @FXML
    public void loadAddBook(ActionEvent actionEvent) {
        loadWindow("/fxml/addbook/add_products.fxml","Add New Book");
    }

    @FXML
    public void loadSettings(ActionEvent actionEvent) {
                loadWindow("/fxml/settings/settings.fxml", "Ustawienie");
    }

    void loadWindow(String location, String title){
        try{
            Parent root = FXMLLoader.load(getClass().getResource(location));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.show();
            LibraryAssistantUtil.setStageIcon(stage);
        }
        catch (IOException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void loadBookInfo(ActionEvent event){
        clearBookCache();
        String id = bookIDInput.getText();
        String query = "SELECT * FROM BOOK WHERE id ='"+id+"'";

        ResultSet resultSet = databaseHandler.execQuery(query);
        Boolean flag = false;
        try{
            while (resultSet.next()){
                String bName = resultSet.getString("title");
                String bAuthor = resultSet.getString("author");
                Boolean bStatus = resultSet.getBoolean("isAvail");
                bookName.setText(bName);
                bookAuthor.setText(bAuthor);
                String status = (bStatus)? "Available":"Not Available";
                bookStatus.setText(status);
                flag = true;
            }
            if (!flag){
                bookName.setText("Not such book available");
            }
        }
        catch (SQLException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private  void loadMemberInfo(ActionEvent event) {
        clearMemberCache();
        String id = memberIDInput.getText();
        String query = "SELECT * FROM MEMBER WHERE id ='"+id+"'";

        ResultSet resultSet = databaseHandler.execQuery(query);
        Boolean flag = false;
        try{
            while (resultSet.next()){
                String mName = resultSet.getString("name");
                String mMobile = resultSet.getString("mobile");

                memberName.setText(mName);
                memberMobile.setText(mMobile);
                flag = true;
            }
            if (!flag){
                bookName.setText("Not such member available");
            }
        }
        catch (SQLException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void clearBookCache(){
        bookName.setText("");
        bookAuthor.setText("");
        bookStatus.setText("");
    }
    void clearMemberCache(){
        memberName.setText("");
        memberMobile.setText("");
    }

    @FXML
    private void loadIssueOperation(ActionEvent event) {
        String memberID = memberIDInput.getText();
        String bookID = bookIDInput.getText();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Issue Operation");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure want to issue the book "+ bookName.getText()+"\n to "+memberName.getText()+"?");

        Optional<ButtonType> response = alert.showAndWait();
        if(response.get()==ButtonType.OK){
            String str ="INSERT INTO ISSUE(memberID,bookID) VALUES (" +
                    "'"+memberID+"'," +
                    "'"+bookID+"')";
            String str2="UPDATE BOOK SET isAvail = false WHERE id = '"+bookID+"'";
            if (databaseHandler.execAction(str)&&databaseHandler.execAction(str2)){
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                alert2.setTitle("Success");
                alert2.setHeaderText(null);
                alert2.setContentText("Book Issue Complete");
                alert2.showAndWait();
            }
            else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR);
                alert2.setTitle("Failed");
                alert2.setHeaderText(null);
                alert2.setContentText("Issue Operation Failed");
                alert2.showAndWait();
            }
        }
        else {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setTitle("Cancelled");
            alert2.setHeaderText(null);
            alert2.setContentText("Issue Operation cancelled");
            alert2.showAndWait();
        }
    }

    @FXML
    private void bookLoadInfo2(ActionEvent event) {
        ObservableList<String> issueData = FXCollections.observableArrayList();
        isReadyForSubmission = false;
        String id = bookID.getText();
        String query = "SELECT * FROM ISSUE WHERE bookID = '"+id+"'";
        ResultSet resultSet = databaseHandler.execQuery(query);
        try{
            while (resultSet.next()){
                String mBookID = id;
                String mMemberID = resultSet.getString("memberID");
                Timestamp mIssueTime = resultSet.getTimestamp("issueTime");
                int mRenewCount = resultSet.getInt("renew_count");

                issueData.add("Issue Data and time : "+mIssueTime.toGMTString());
                issueData.add("Renew Count : "+mRenewCount);
                issueData.add("Book Information:- ");
                query = "SELECT * FROM BOOK WHERE ID = '"+ mBookID +"'";
                System.out.println(mBookID);

                ResultSet rl = databaseHandler.execQuery(query);

                while (rl.next()){
                    issueData.add("\tBook name: "+rl.getString("title"));
                    issueData.add("\tBook id: "+rl.getString("id"));
                    issueData.add("\tBook author: "+rl.getString("author"));
                    issueData.add("\tBook publisher: "+rl.getString("publisher"));
                }
                query = "SELECT * FROM MEMBER WHERE ID = '"+mMemberID+"'";
                rl = databaseHandler.execQuery(query);
                issueData.add("Member Information:- ");
                while (rl.next()){
                    issueData.add("\tName: "+ rl.getString("name"));
                    issueData.add("\tMobile: "+ rl.getString("mobile"));
                    issueData.add("\tEmail: "+ rl.getString("email"));
                }
                isReadyForSubmission = true;
            }
        }
        catch (SQLException e){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, e);
        }
        issueDataList.getItems().setAll(issueData);
    }

    @FXML
    private void loadSubmissionOP(ActionEvent event) {
        if (!isReadyForSubmission) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed");
            alert.setHeaderText(null);
            alert.setContentText("Please select a book to submit");
            alert.showAndWait();
            return;
        }

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Submission Operation");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure want to return the book ?");

        Optional<ButtonType> response = alert.showAndWait();
        if (response.get() == ButtonType.OK) {
            String id = bookID.getText();
            String query = "DELETE FROM ISSUE WHERE BOOKID = '" + id + "'";
            String query2 = "UPDATE BOOK SET ISAVAIL = TRUE WHERE ID = '" + id + "'";
            if (databaseHandler.execAction(query) && databaseHandler.execAction(query2)) {
                Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText(null);
                alert.setContentText("Book has been submitted");
                alert.showAndWait();
            } else {
                Alert alert1 = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Failed");
                alert.setHeaderText(null);
                alert.setContentText("Submission has been failed");
                alert.showAndWait();
            }
        }
        else {
            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
            alert1.setTitle("Cancelled");
            alert1.setHeaderText(null);
            alert1.setContentText("Submission Operation canceled");
            alert1.showAndWait();
        }
    }

    public void loadRenewOP(ActionEvent event) {
        if (!isReadyForSubmission) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Failed");
            alert.setHeaderText(null);
            alert.setContentText("Please select a book to renew");
            alert.showAndWait();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Renew Operation");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure want to renew the book ?");

        Optional<ButtonType> response = alert.showAndWait();
        if (response.get() == ButtonType.OK) {
            String query = "UPDATE ISSUE SET issueTime = CURRENT_TIMESTAMP, renew_count = renew_count+1 WHERE BOOKID = '"+bookID.getText()+"'";
            if(databaseHandler.execAction(query)){
                Alert alert1= new Alert(Alert.AlertType.INFORMATION);
                alert1.setTitle("Success");
                alert1.setHeaderText(null);
                alert1.setHeaderText("Book has been renew");
                alert1.showAndWait();
            }
         else {
            Alert alert1 = new Alert(Alert.AlertType.ERROR);
            alert1.setTitle("Failed");
            alert1.setHeaderText(null);
            alert1.setContentText("Renew has been failed");
            alert1.showAndWait();
        }
        }
        else {
            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
            alert1.setTitle("Cancelled");
            alert1.setHeaderText(null);
            alert1.setContentText("Renew Operation canceled");
            alert1.showAndWait();
        }
    }


    @FXML
    private void handlerMenuClose(ActionEvent event) {
        ((Stage)rootPane.getScene().getWindow()).close();
    }

    @FXML
    private void handlerMenuAddBook(ActionEvent event) {
        loadWindow("/fxml/addbook/add_products.fxml","Add New Book");
    }

    @FXML
    private void handlerMenuAddMember(ActionEvent event) {
        loadWindow("/fxml/addmember/member_add.fxml","Add New Member");
    }

    public void handlerMenuViewBooks(ActionEvent event) {
        loadWindow("/fxml/listbook/products_list.fxml","Book List");
    }

    public void handlerMenuViewMembers(ActionEvent event) {
        loadWindow("/fxml/listmember/member_list.fxml","Member List");
    }

    public void handlerMenuFullScreen(ActionEvent event) {
        Stage stage = ((Stage) rootPane.getScene().getWindow());
        stage.setFullScreen(!stage.isFullScreen());
    }

    public void initDrawer(){

    }
}
