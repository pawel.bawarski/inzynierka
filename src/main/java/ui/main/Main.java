package ui.main;

import datebase.DatabaseHandler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.LibraryAssistantUtil;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/login/login.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        stage.setTitle("Logowanie do magazynu");

        LibraryAssistantUtil.setStageIcon(stage);

        new Thread(() -> DatabaseHandler.getInstance()).start();
    }

    public static void main(String[] args) {
        launch(args);
    }

}