package ui.login;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.codec.digest.DigestUtils;
import settings.Preferances;
import ui.main.MainController;
import util.LibraryAssistantUtil;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController implements Initializable {

    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;

    Preferances preferances;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        preferances = Preferances.;
    }
    @FXML
    public void handlerLoginButtonAction(ActionEvent event) {

        String uname = username.getText();
        String pword = DigestUtils.sha1Hex(password.getText());

        if (uname.equals(preferances.getUsername())&&pword.equals(preferances.getPassword())){
            closeStage();
            loadWindow();
        }
        else {
            username.getStyleClass().add("wrong-credentials");
            password.getStyleClass().add("wrong-credentials");
        }
    }

    @FXML
    public void handlerCancelButtonAction(ActionEvent event) {
        System.exit(0);
    }

    public void closeStage(){
        ((Stage)username.getScene().getWindow()).close();
    }

    void loadWindow(){
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/main/main.fxml"));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle("Magazyn");
            stage.setScene(new Scene(root));
            stage.show();
            LibraryAssistantUtil.setStageIcon(stage);
        }
        catch (IOException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
